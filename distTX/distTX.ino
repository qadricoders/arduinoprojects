#include <Servo.h>


#define trigPin 11
#define echoPin 13

const int transmit_pin = 6;
const int led = 9;
const int buzzerPin = 8;
const int servoMtrpin = 4;
const float cmToFeet = 0.032;//30.48;
const float distWall = 140;
long roomSettings[180];
const int servMovementDelay = 200;
const int echoDelay = 1500;
const int incrDeg = 15;
const int maxDeg = 160;
//RFEasy transmitter;
Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int sPos = 0;   // variable to store the servo position
long txCount = 1;
int pos = 0;
boolean shouldScan = true;
int lastServPos = 165;

void setup() {
  Serial.begin(9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(led, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  myservo.attach(servoMtrpin);

  //transmitter.init_transmitter(transmit_pin);


  for ( pos = 0; pos < maxDeg; pos += incrDeg) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);
    delay(servMovementDelay);// tell servo to go to position in variable 'pos'
    long dist = calculateDistance();
    delay(echoDelay);
    Serial.print("Setup Pos:[");
    Serial.print(pos);
    Serial.print("]:");
    Serial.println(dist);
    roomSettings[pos] = dist;
    if (pos >= lastServPos ) {
      //  myservo.write(0);
      break;
    }
  }
  Serial.println("---- setup complete ...");
  delay (100);
}

void loop() {

  if (shouldScan) {
    scanRoom();
  }

  //transmitData();
  delay(100);

}

boolean scanRoom() {

  
  for ( pos = 0; pos < maxDeg; pos += incrDeg) { // goes from 180 degrees to 0 degrees

    myservo.write(pos);
    delay(servMovementDelay);
    long distNow = calculateDistance();
    delay(echoDelay);
    long diff = distNow - roomSettings[pos];
    Serial.print("Servpos:");  Serial.println(pos);
    Serial.print(" Pos value:["); Serial.print(roomSettings[pos]);
    Serial.print("]- Actual:");  Serial.println(distNow);
    Serial.print(" DIFF ..."); Serial.println(diff);
    if (diff > 4 || diff < -4) {
      tone(buzzerPin, 1000, 190);//startbuzzer
      digitalWrite(led, HIGH);//start led
      shouldScan = false;//stop scanning untill obstruction is clear
      boolean wtStatus = waitForAutoCorrection();
      if (!wtStatus) {
       
       return false;
      }
      
    }
  }//end for
  return true;
}


boolean waitForAutoCorrection() {

  for (int rfor = 0; rfor < 3; rfor++) {

    long distNow = calculateDistance();
    delay(echoDelay);
    long diff =   roomSettings[pos] - distNow;
    if (diff <= 4) {
      Serial.print("shouldScan false : Array pos:"); Serial.println(pos);
      shouldScan = true;
      Serial.print(" Pos value:["); Serial.print(roomSettings[pos]);
      Serial.print("]- Actual:"); Serial.println(distNow);
      Serial.println("Corrected dist ...");
      Serial.print(diff);  Serial.println("Continue ...");
      digitalWrite(led, LOW);//stop led
      return true;
    } else {
      Serial.print("uncorr Array pos:"); Serial.print(pos);
      Serial.print(" Difference :"); Serial.print(diff);
      Serial.print(" Actual val :["); Serial.print(distNow);
      Serial.print("] Expected val"); Serial.println(roomSettings[pos]);
      return false;
    }
  }


}



String getDistance(long dist) {
  String retVal = "";
  String ft = String("Feet ");
  String cm = String("cm ");
  if (dist > 60.96) { //2feet
    float conv = dist * cmToFeet;
    retVal = conv + ft;
    // Serial.println(retVal);
    return retVal;
  }
  else {
    retVal = dist + cm;
    // Serial.println(retVal);
    return retVal;
  }
}


long calculateDistance() {

  //Serial.println(sensorValue);
  long duration, distance, actualDist;


  digitalWrite(trigPin, LOW);
  delayMicroseconds(40);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(40);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;
  actualDist = (duration / 2);
  return distance;
}

void transmitData() {
  int sensorValue = analogRead(A0);
 Serial.print("Send...");
  String stringOne = "LightValue: ";
  String stringThree = stringOne + sensorValue;
 // transmitter.transmit(stringThree);
    Serial.println("...|");
    txCount++;



}


