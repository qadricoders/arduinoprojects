
#include <VirtualWire.h>
//this code is to be used in living room switching on two lights 

int count;
#define rxPin 5
int pirPin = 7;
int switch1 = 2;
int switch2 = 6;
int pirState = LOW;
int val = 0; // variable for readin pin status
boolean trigByCode = false;
int lessLight = 1005;//1018
int enoughLight = 950;
long twoMins = 180000; //120000 - 2 min , 180000 - 3 min
unsigned  long twoMinpreviousTime = 0;
unsigned int data = 0;
const String motionMsg = "motion";
const int transmit_en_pin = 3;
const int transmit_pin = 12;


void setup() {
  Serial.begin(9600);
  Serial.println("setup......");
  pinMode(pirPin, INPUT);
  pinMode(switch1, OUTPUT);
  pinMode(switch2, OUTPUT);
  twoMinpreviousTime = millis();

  digitalWrite(switch1, HIGH);
  digitalWrite(switch2, HIGH);
  // Initialise the IO and ISR
   vw_set_tx_pin(transmit_pin);
   vw_set_ptt_pin(transmit_en_pin);
   vw_set_ptt_inverted(true); // Required for DR3100
   vw_setup(2000);  // Bits per sec
   vw_rx_start();       // Start the receiver PLL running
}

void loop() {
  //data=analogRead(rfReceivePin);
    Serial.println("in loop......");
    unsigned  long currTime = millis();
    
    val = digitalRead(pirPin);
    boolean isMoveDetected = isMotionDetected(val);
    Serial.print("PIR Activated:");    Serial.println(isMoveDetected);

    int lightSensor = analogRead(A0);
    Serial.print("illumination  VAL:");  Serial.println(lightSensor);

  if (needlight(lightSensor) &&  isMoveDetected) { // if its dark  and motion detected switch on lights
    Serial.println("needlight true && movedetected ");
    digitalWrite(switch1, LOW);
    digitalWrite(switch2, LOW);

    trigByCode = true;
  } else if ((trigByCode && isBrightEnough(lightSensor)) || noMotionForTwoMins(val, currTime)) { //condition to switch off the light
    // if code switched on the light and its very bright (daylight)
    Serial.println("Good bye!");
    digitalWrite(switch1, HIGH);
    digitalWrite(switch2, HIGH);
    trigByCode = false;
  }

  delay(2000);
  Serial.println("off");
  //digitalWrite(switch1, HIGH);
  //digitalWrite(switch2, HIGH);
}

boolean needlight(int lightSensor) {
  if ( lightSensor >= lessLight ) { // high number from sensor  is More DARK !!
    return true;
  } else {
    return false;
  }
}

boolean isBrightEnough(int lightSensor) {
  if ( lightSensor <= enoughLight ) {
    return true;
  } else {
    return false;
  }
}

boolean isMotionDetected(int val) {
  if ( val == HIGH ) {
    twoMinpreviousTime = millis();
    return true;
  } else {
    return false;
  }

}

boolean noMotionForTwoMins(int val , unsigned long currTime) {
  unsigned long gap = currTime - twoMinpreviousTime ;
  if ( !isMotionDetected(val) && gap >= twoMins) {
    twoMinpreviousTime = currTime;
    Serial.println("No motion for set duration");
    return true;
  } else {
    return false;
  }
}

