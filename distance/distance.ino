
#define trigPin 12
#define echoPin 13
int led = 9;
int buzzerPin = 8;


void setup() {
 Serial.begin(9600);
 pinMode(trigPin, OUTPUT);
 pinMode(echoPin, INPUT);
 pinMode(led, OUTPUT);
 pinMode(buzzerPin, OUTPUT);
 
}

void loop() {
 int sensorValue=analogRead(A0);
 Serial.println(sensorValue);
 long duration, distance,actualDist;
 float cmToFeet = 30.48;
 float distWall = 166;

 digitalWrite(trigPin, LOW);
 delayMicroseconds(10);
 digitalWrite(trigPin, HIGH);
 delayMicroseconds(10);
 digitalWrite(trigPin, LOW);
 duration = pulseIn(echoPin, HIGH);
 distance = (duration/2)/29.1;
 actualDist = (duration/2);
// Serial.println(actualDist);
 
 
 if(distance < distWall){ // detects obstruction 
    digitalWrite(led, HIGH);
    tone(buzzerPin,1000,190);
    delay(10);
    
    }else{
      digitalWrite(led, LOW);
      
     }
 if (distance >60.96){//2feet
  Serial.print(distance/cmToFeet);
  Serial.println("Feet");  
  }
else{
  Serial.print(distance);
  Serial.println("cm");
   }
 delay(500); 
}



